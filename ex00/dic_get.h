/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_get.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 18:47:25 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 19:33:28 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIC_GET_H
# define DIC_GET_H

# include "dic.h"

char	*get_name(t_dic_entry **dic, int base, int exponent);

#endif
