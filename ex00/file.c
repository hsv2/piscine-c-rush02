/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 12:18:18 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 21:26:45 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#include "string_func.h"
#include "line_free.h"

ssize_t	get_file_size(int fd)
{
	char	buffer[256];
	ssize_t	size;
	ssize_t	size_read;

	size = 0;
	size_read = 0;
	size_read = read (fd, buffer, sizeof buffer);
	size = size_read;
	while (size_read > 0)
	{
		size_read = read (fd, buffer, sizeof buffer);
		size += size_read;
	}
	return (size);
}

char	*read_file(char *filename)
{
	ssize_t	size;
	char	*file_content;
	int		fd;
	int		offset;

	file_content = 0;
	fd = open(filename, O_RDONLY);
	if (fd <= 0)
		return (0);
	file_content = malloc(get_file_size(fd) + 1);
	close(fd);
	if (!file_content)
		return (0);
	fd = open(filename, O_RDONLY);
	if (!fd)
		return (0);
	offset = 0;
	size = read(fd, file_content + offset, 64);
	while (size > 0)
	{
		offset += size;
		size = read(fd, file_content + offset, 64);
	}
	close(fd);
	return (file_content);
}

char	**read_and_get_line(char *filename)
{
	char	*file_content;
	char	**lines;

	file_content = read_file(filename);
	if (!file_content)
		return (0);
	lines = get_lines(file_content);
	free(file_content);
	return (lines);
}
