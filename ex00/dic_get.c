/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_get.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 19:16:55 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 21:11:39 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dic_get.h"
#include "dic_struct.h"

char	*get_name(t_dic_entry **dic, int base, int exponent)
{
	int	i;

	i = 0;
	while (dic[i] != 0)
	{
		if (dic[i]->base == base && dic[i]->exponent == exponent)
			return (dic[i]->name);
		i++;
	}
	return (0);
}
