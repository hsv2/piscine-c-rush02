/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 19:58:44 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 22:01:12 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "dic.h"
#include "dic_struct.h"
#include "dic_parse.h"
#include "string_func.h"
#include "file.h"
#include "line_free.h"
#include "parse_number.h"
#include "dic_more.h"

t_dic_entry	*create_entry(char *number, char *name)
{
	t_dic_entry	*entry;

	entry = malloc(sizeof (t_dic_entry));
	entry->name = malloc(str_len(name) + 1);
	entry->base = number_base(number);
	entry->exponent = number_exponent(number);
	str_copy(entry->name, name);
	return (entry);
}

int	count_array(char **array)
{
	int	len;

	len = 0;
	while (*array++ != 0)
		len++;
	return (len);
}

t_dic_entry	**load_dictionary(char *filename)
{
	char		**lines;
	t_dic_entry	**dic;

	lines = read_and_get_line(filename);
	if (!lines)
		return (0);
	dic = malloc((count_array(lines) + 1) * sizeof(t_dic_entry *));
	if (!dic)
	{
		free_lines(lines);
		return (0);
	}
	if (!fill_dic(lines, dic))
		return (0);
	free_lines(lines);
	return (dic);
}

void	free_entry(t_dic_entry *entry)
{
	free(entry->name);
	free(entry);
}

void	free_dictionary(t_dic_entry **dic)
{
	int	i;

	i = 0;
	while (dic[i] != 0)
	{
		free(dic[i]);
		i++;
	}
	free(dic);
}
