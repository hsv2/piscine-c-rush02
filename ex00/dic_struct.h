/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_struct.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: roberodr <roberodr@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 12:58:15 by roberodr          #+#    #+#             */
/*   Updated: 2022/04/17 19:13:36 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIC_STRUCT_H
# define DIC_STRUCT_H

struct s_dic_entry
{
	int		base;
	int		exponent;
	char	*name;
};

#endif
