/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_to_name.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 19:42:20 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 20:39:45 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NUMBER_TO_NAME_H
# define NUMBER_TO_NAME_H

# include "dic.h"

void	print_number_name(t_dic_entry **dic, char *nbr);

#endif
