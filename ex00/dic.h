/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 19:58:46 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 16:00:49 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIC_H
# define DIC_H

struct						s_dic_entry;
typedef struct s_dic_entry	t_dic_entry;

t_dic_entry	*create_entry(char *number, char *name);
t_dic_entry	**load_dictionary(char *filename);
void		free_entry(t_dic_entry *entry);
void		free_dictionary(t_dic_entry **dic);

#endif
