/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_more.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 21:57:55 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 22:03:48 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dic_more.h"
#include "dic_parse.h"

int	fill_dic(char **lines, t_dic_entry **dic)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (lines[j] != 0)
	{
		if (!valid_dic_line(lines[j]))
			return (0);
		dic[i] = dic_parse_line(lines[j]);
		if (dic[i])
			i++;
		j++;
	}
	return (1);
}
