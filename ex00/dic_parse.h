/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_parse.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 12:08:37 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 21:52:16 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIC_PARSE_H
# define DIC_PARSE_H

# include "dic.h"

t_dic_entry	*dic_parse_line(char *line);
int			valid_dic_line(char *line);

#endif
