/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_func.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 19:47:08 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 19:35:12 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "string_func.h"
#include "string_class.h"

int	count_lines(char *text)
{
	int	lines;

	if (!text)
		return (0);
	lines = 1;
	while (*text != 0)
		if (*text++ == '\n')
			lines++;
	return (lines);
}

char	**get_lines(char *text)
{
	char	**lines;
	char	*iter;
	int		i;
	int		j;

	if (!text)
		return (0);
	lines = malloc((count_lines(text) + 1) * sizeof (char *));
	i = 0;
	iter = text;
	while (i < count_lines(text))
	{
		j = 0;
		lines[i] = malloc(line_len(iter) + 1);
		while (j < line_len(iter))
		{
			lines[i][j] = iter[j];
			j++;
		}
		lines[i][j] = 0;
		iter += line_len(iter) + 1;
		i++;
	}
	lines[count_lines(text)] = 0;
	return (lines);
}

int	line_len(char *str)
{
	int	len;

	if (!str)
		return (0);
	len = 0;
	while (*(str + len) != 0 && *(str + len) != '\n')
		len++;
	return (len);
}

int	str_len(char *str)
{
	int	len;

	if (!str)
		return (0);
	len = 0;
	while (*str++ != 0)
		len++;
	return (len);
}

char	*str_copy(char *dst, char *src)
{
	char	*start;

	if (!src)
		return (0);
	start = dst;
	while (*src != 0)
	{
		if (!ft_isprintable(*src))
			return (0);
		*dst = *src;
		src++;
		dst++;
	}
	*dst = 0;
	return (start);
}
