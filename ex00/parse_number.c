/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_number.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 11:26:09 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 21:11:39 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include "parse_number.h"
#include "string_class.h"
#include "math.h"

char	*number_start(char *str)
{
	while (*str != '\0')
	{
		if (*str == '0' && (*(str + 1) < '0' || *(str + 1) > '9'))
			return (str);
		if (*str >= '1' && *str <= '9')
			return (str);
		if (!(ft_isspace(*str) || *str == '0' || (*(str + 1) >= '0'
					&& *(str + 1) <= '9' && *str == '+')))
			return (0);
		str++;
	}
	return (0);
}

int	number_len(char *str)


{
	int	len;

	len = 0;
	while (str[len] >= '0' && str[len] <= '9')
	{
		len++;
	}
	return (len);
}

int	number_exponent(char *nbr)
{
	int	exp;
	int	i;

	exp = 0;
	i = number_len(nbr) - 1;
	if (!i)
		return (0);
	while (i >= 0)
	{
		if (nbr[i] == '0')
			exp++;
		else
			break ;
		i--;
	}
	return (exp);
}

int	number_base(char *nbr)
{
	int	exp;
	int	res;
	int	i;

	exp = 0;
	res = 0;
	i = number_len(nbr) - 1 - number_exponent(nbr);
	while (i >= 0)
	{
		res += (nbr[i] - '0') * ft_pow(10, exp);
		exp++;
		i--;
	}
	return (res);
}

char	*parse_number(char *str, char **end)
{
	char	*nbr;
	int		i;

	i = 0;
	str = number_start(str);
	if (!str)
		return (0);
	nbr = malloc(number_len(str) + 1);
	while (i < number_len(str))
	{
		nbr[i] = str[i];
		i++;
	}
	nbr[i] = 0;
	if (end)
		*end = &str[i];
	return (nbr);
}
