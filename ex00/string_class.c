/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_class.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 12:23:50 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 16:44:50 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isspace(char c)
{
	return (c == '\n' || c == '\t' || c == '\v' || c == '\f'
		|| c == '\r' || c == ' ');
}

int	ft_isdigit(char c)
{
	return (c >= '0' && c <= '9');
}

int	ft_isprintable(char c)
{
	return (c >= ' ' && c <= '~');
}

char	*skip_space(char *str)
{
	if (!str)
		return (0);
	while (ft_isspace(*str))
		str++;
	return (str);
}
