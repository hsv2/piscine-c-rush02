/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_free.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 17:06:18 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 17:06:57 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "line_free.h"

void	free_lines(char **lines)
{
	unsigned int	i;

	i = 0;
	while (lines[i] != 0)
	{
		free(lines[i]);
		i++;
	}
	free(lines);
}
