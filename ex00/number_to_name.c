/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_to_name.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 19:42:25 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 22:17:50 by roberodr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "number_to_name.h"
#include "parse_number.h"
#include "math.h"
#include "dic_get.h"

void	print_str(char *str)
{
	while (*str != 0)
		write(STDOUT_FILENO, str++, 1);
}

void	print_hundreds(t_dic_entry **dic, int nbr)
{
	if (nbr > 99)
	{
		print_str(get_name(dic, nbr / 100, 0));
		print_str(" ");
		print_str(get_name(dic, 1, 2));
		if (nbr % 100)
			print_str(" ");
	}
	nbr -= nbr / 100 * 100;
	if (nbr > 19)
	{
		print_str(get_name(dic, nbr / 10, 1));
		if (nbr % 10)
			print_str(" ");
	}
	else if (nbr > 10)
	{
		print_str(get_name(dic, nbr, 0));
		return ;
	}
	else if (nbr == 10)
		print_str(get_name(dic, 1, 1));
	nbr = nbr % 10;
	if (nbr)
		print_str(get_name(dic, nbr, 0));
}

int	get_hundreds(char *nbr, int thousand_exp, char **end)
{
	int	i;
	int	hundreds;
	int	exp;

	i = 0;
	hundreds = 0;
	while (i < number_len(nbr) - 3 * thousand_exp)
	{
		exp = number_len(nbr) - 3 * thousand_exp - 1 - i;
		hundreds += (nbr[i] - '0') * ft_pow(10, exp);
		i++;
	}
	*end += i;
	return (hundreds);
}

void	print_number_name(t_dic_entry **dic, char *nbr)
{
	int	thousand_exp;
	int	hundreds;

	if (number_len(nbr) == 1 && *nbr == '0')
	{
		print_str(get_name(dic, 0, 0));
		return ;
	}
	while (*nbr != 0)
	{
		thousand_exp = (number_len(nbr) - 1) / 3;
		hundreds = get_hundreds(nbr, thousand_exp, &nbr);
		print_hundreds(dic, hundreds);
		if (thousand_exp > 0)
		{
			if (hundreds)
				print_str(" ");
			print_str(get_name(dic, 1, thousand_exp * 3));
			write(STDOUT_FILENO, " ", 1);
		}
	}
}
