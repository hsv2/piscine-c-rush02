/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_class.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 12:23:45 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 15:31:18 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRING_CLASS_H
# define STRING_CLASS_H

int		ft_isspace(char c);
int		ft_isprintable(char c);
int		ft_isdigit(char c);
char	*skip_space(char *str);

#endif
