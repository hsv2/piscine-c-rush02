/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_more.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 21:54:36 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 21:57:39 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIC_MORE_H
# define DIC_MORE_H

# include "dic.h"

int	fill_dic(char **lines, t_dic_entry **dic);

#endif
