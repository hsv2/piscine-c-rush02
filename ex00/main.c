/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 20:44:44 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 22:18:51 by roberodr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "dic.h"
#include "number_to_name.h"
#include "parse_number.h"

int	print_error(void)
{
	write(STDOUT_FILENO, "Error\n", 6);
	return (-1);
}

int	run(char *filename, char *nbr)
{
	t_dic_entry	**dic;

	dic = load_dictionary(filename);
	if (!dic)
	{
		write(STDOUT_FILENO, "Dict Error\n", 11);
		return (-1);
	}
	print_number_name(dic, nbr);
	write(STDOUT_FILENO, "\n", 1);
	return (0);
}

int	main(int argc, char **argv)
{
	char		*nbr;

	if (argc == 2)
	{
		nbr = parse_number(argv[1], 0);
		if (!nbr)
			return (print_error());
		return (run("numbers.dict", nbr));
	}
	else if (argc == 3)
	{
		nbr = parse_number(argv[2], 0);
		if (!nbr)
			return (print_error());
		return (run(argv[1], nbr));
	}
	else
		return (print_error());
}
