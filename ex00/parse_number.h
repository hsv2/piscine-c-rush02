/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_number.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 11:26:13 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 19:16:12 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSE_NUMBER_H
# define PARSE_NUMBER_H

char	*parse_number(char *str, char **end);
int		number_len(char *str);
int		number_exponent(char *nbr);
int		number_base(char *nbr);

#endif
