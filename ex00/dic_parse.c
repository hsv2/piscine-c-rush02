/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_parse.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: roberodr <roberodr@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 12:54:05 by roberodr          #+#    #+#             */
/*   Updated: 2022/04/17 21:50:12 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "dic.h"
#include "dic_parse.h"
#include "parse_number.h"
#include "string_class.h"
#include "string_func.h"

char	*skip_colon(char *str)
{
	if (*str == ':')
		return (str + 1);
	return (0);
}

char	*skip_delimitors(char *str)
{
	str = skip_space(str);
	str = skip_colon(str);
	if (!str)
		return (0);
	str = skip_space(str);
	return (str);
}

t_dic_entry	*dic_parse_line(char *line)
{
	char		*number;
	char		*name;
	t_dic_entry	*entry;

	if (str_len(line) < 2)
		return (0);
	number = parse_number(line, &line);
	if (!number)
		return (0);
	line = skip_delimitors(line);
	if (!line)
		return (0);
	name = malloc(str_len(line) + 1);
	if (!name)
		return (0);
	if (!str_copy(name, line))
	{
		free(name);
		return (0);
	}
	if (!name || !str_len(name))
		return (0);
	entry = create_entry(number, name);
	free(number);
	return (entry);
}

int	valid_dic_line(char *line)
{
	char	*ptr;

	if (str_len(line) < 2)
		return (1);
	ptr = parse_number(line, &line);
	if (!ptr)
		return (0);
	line = skip_delimitors(line);
	while (*line != 0)
	{
		if (!ft_isprintable(*line))
			return (0);
		line++;
	}
	return (1);
}
