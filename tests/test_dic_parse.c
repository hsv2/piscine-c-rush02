/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_dic_parse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 14:45:43 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 15:15:31 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "../ex00/dic_parse.h"
#include "../ex00/dic_struct.h"

int	main(void)
{
	t_dic_entry	*entry;

	entry = dic_parse_line("10 : ten");
	assert(entry);
	assert(strcmp(entry->number, "10") == 0);
	assert(strcmp(entry->name, "ten") == 0);
	entry = dic_parse_line("10   \t:\t   ten");
	assert(entry);
	assert(strcmp(entry->number, "10") == 0);
	assert(strcmp(entry->name, "ten") == 0);
	entry = dic_parse_line("10 : ");
	assert(!entry);
	entry = dic_parse_line(" 10 : ten");
	assert(entry);
	assert(strcmp(entry->number, "10") == 0);
	assert(strcmp(entry->name, "ten") == 0);
	entry = dic_parse_line("0010 : ten");
	assert(entry);
	assert(strcmp(entry->number, "10") == 0);
	assert(strcmp(entry->name, "ten") == 0);
	return (0);
}
