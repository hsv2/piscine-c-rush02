/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_parse_number.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 14:46:41 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 14:47:18 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <assert.h>
#include <string.h>
#include "../ex00/parse_number.h"

int	main(void)
{
	assert(parse_number("    -100") == 0);
	assert(parse_number("") == 0);
	assert(parse_number(" ") == 0);
	assert(parse_number("a1") == 0);
	assert(strcmp(parse_number("  +100"), "100") == 0);
	assert(strcmp(parse_number("  +0"), "0") == 0);
	assert(strcmp(parse_number("  100"), "100") == 0);
	assert(strcmp(parse_number("  10000000000abc"), "10000000000") == 0);
	return (0);
}
