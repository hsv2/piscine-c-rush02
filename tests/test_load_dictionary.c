/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_load_dictionary.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 19:35:39 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 19:35:40 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <assert.h>
#include <stdio.h>

#include "../ex00/dic.h"
#include "../ex00/dic_struct.h"

int	main(void)
{
	t_dic_entry	**dic;

	dic = load_dictionary("numbers.dict");
	assert(dic);
	while (*dic != 0)
	{
		printf("'%s'\t'%s'\n", (*dic)->name, (*dic)->number);
		dic++;
	}
	return (0);
}
