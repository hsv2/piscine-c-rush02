/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_dic_get.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 19:21:11 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 19:25:05 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ex00/dic_get.h"
#include <stdio.h>

int	main(void)
{
	t_dic_entry	**dic;

	dic = load_dictionary("numbers.dict");
	printf("name of 100: %s\n", get_name(dic, 1, 2));
	return (0);
}
