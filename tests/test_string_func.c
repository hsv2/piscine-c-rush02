/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_string_func.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 14:47:51 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 14:49:11 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <assert.h>
#include <string.h>

#include "../ex00/string_func.h"

int	main(void)
{
	char	**lines;

	assert(count_lines("") == 1);
	assert(count_lines("abc") == 1);
	assert(count_lines("abc\n") == 2);
	assert(count_lines("abc\nabcd\nabcd") == 3);
	assert(line_len("abc\nefg\nhij") == strlen("abc"));
	assert(line_len("abc") == strlen("abc"));
	assert(line_len("") == strlen(""));
	lines = get_lines("test\nhello");
	assert(lines[2] == 0);
	assert(strcmp("test", lines[0]) == 0);
	assert(strcmp("hello", lines[1]) == 0);
	return (0);
}
